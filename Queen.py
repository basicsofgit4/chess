from rook import rook_moves
from bishop import alpha_to_num , positions_of_bishops , bishop_moves

def queen_moves(position: str) -> list[str]: 
    queen = rook_moves(position)
    queen.extend(bishop_moves(position))
    return sorted(queen)

#print(queen_moves('a6'))
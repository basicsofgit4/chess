
files = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h']
ranks = [1, 2, 3, 4, 5, 6, 7, 8]

def is_valid(row: str, column: int) -> bool:
    if row in files:
        if column in ranks:
            return True
        else:
            return False

def knight_moves(position: str) -> list:
    possible_cordinates = []
    possible_movements = [(2, 1), (2, -1), (-2, 1), (-2, -1), (1, 2), (-1, 2), (1, -2), (-1, -2)]

    row = ord(position[0]) - ord('a') + 1
    column = int(position[1])  

    for move_point in possible_movements:
        new_row = chr(row + move_point[1] + ord('a') - 1)
        new_column =  column + move_point[0]        

        if is_valid(new_row, new_column):
            new_cordinates = new_row + str(new_column)
            possible_cordinates.append(new_cordinates)
    
    return possible_cordinates

#print(knight("a8"))

    



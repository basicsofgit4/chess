def rook_moves(start: str) -> list[str]:
    col = (ord(start[0].lower()) - ord('a'))
    row = int(start[1])
    possible_positions = []
    for i in range(1, row):
        possible_positions.append(chr(ord('a') + col) + str(i))

    for i in range(row + 1, 9):
        possible_positions.append(chr(ord('a') + col) + str(i))

    for i in range(1, col + 1):
        possible_positions.append(chr((ord('a') + i) - 1) + str(row))

    for i in range(col + 2, 9):
        possible_positions.append(chr((ord('a') + i) - 1) + str(row))

    return possible_positions